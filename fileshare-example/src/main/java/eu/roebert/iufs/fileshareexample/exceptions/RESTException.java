package eu.roebert.iufs.fileshareexample.exceptions;

import java.util.ArrayList;

public class RESTException {
    private ArrayList<String> errors;

    public RESTException(String errorMessage) {
        errors = new ArrayList<String>();
        errors.add(errorMessage);
    }

    public RESTException(ArrayList<String> errorMessages) {
        errors = errorMessages;
    }

    public ArrayList<String> getError() {
        return errors;
    }
}
