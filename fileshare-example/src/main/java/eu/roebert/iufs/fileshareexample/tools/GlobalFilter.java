package eu.roebert.iufs.fileshareexample.tools;

import java.util.HashMap;

import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

/**
 * Really simple helper for enabling CORS in a spark application;
 */
public final class GlobalFilter {
    private static final HashMap<String, String> headers = new HashMap<String, String>();

    static {
        headers.put("Access-Control-Allow-Methods", "*");
        headers.put("Access-Control-Allow-Origin", "*");
        headers.put("Access-Control-Allow-Headers", "*");
        headers.put("Access-Control-Allow-Credentials", "true");
        headers.put("Content-Encoding", "gzip");
    }

    public final static void apply() {
        Filter filter = new Filter() {
            @Override
            public void handle(Request request, Response response) throws Exception {
                headers.forEach((key, value) -> {
                    response.header(key, value);
                });
            }
        };

        Spark.after(filter);
    }
}
