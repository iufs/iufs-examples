package eu.roebert.iufs.fileshareexample.controller;

import static spark.Spark.get;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.core.IIUFSService;
import eu.roebert.iufs.model.FileContainer;
import eu.roebert.iufs.model.FileID;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import spark.Request;
import spark.Response;

@Agent
@Service
@RequiredServices({ @RequiredService(name = "iufsservice", type = IIUFSService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class DownloadFileControllerAgent extends Controller {
    private static final Logger LOG = LoggerFactory.getLogger(DownloadFileControllerAgent.class);

    @AgentService(lazy = false)
    IIUFSService iufsservice;
    
    @AgentBody
    public void onAgentBody() {
        get("download/:fileHash", "application/json", this::getSparkLogic);
        
        LOG.info("{} Agent Started", this);
    }

    @Override
    public Object getSparkLogic(Request request, Response response) {
        String fileHash = request.params("fileHash");
        params.add("fileHash");

        if (paramsExist(request)) {
            try(FileContainer file = iufsservice.download(new FileID(fileHash)).get()) {
                response.header("Content-disposition", "attachment; filename=" + fileHash + "."+file.getExtension()+";");

                InputStream in = new FileInputStream(file.toFile());
                OutputStream out = response.raw().getOutputStream();
                IOUtils.copy(in, out);
                in.close();
                out.close();

                return response;
            } catch (IOException e) {
                errors.add("File could not be downloaded.");
                e.printStackTrace();
            }
        }

        return generateResponse(response);
    }
}
