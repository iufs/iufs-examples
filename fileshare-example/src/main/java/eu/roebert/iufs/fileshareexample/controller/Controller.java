package eu.roebert.iufs.fileshareexample.controller;

import java.util.ArrayList;

import eu.roebert.iufs.fileshareexample.exceptions.RESTException;
import eu.roebert.iufs.fileshareexample.tools.JsonTransformer;
import spark.Request;
import spark.Response;

public abstract class Controller {
    public static JsonTransformer json = new JsonTransformer();
    protected ArrayList<String> errors = new ArrayList<String>();
    protected ArrayList<String> params = new ArrayList<String>();
    protected Object model;

    public abstract Object getSparkLogic(Request request, Response response);

    protected boolean paramsExist(Request request) {
        errors.clear();
        params.forEach((param) -> {
            if ((request.queryParams(param) == null || request.queryParams(param).equals(""))
                    && request.queryParamsValues(param) == null
                    && (request.params(param) == null || request.params(param).equals(""))) {
                this.errors.add("The " + param + " is missing.");
            }
        });

        return errors.isEmpty();
    }

    protected Object generateResponse(Response response) {
        params.clear();
        if (!this.errors.isEmpty()) {
            response.status(400);
            return new RESTException(this.errors);
        }

        response.status(200);
        return model;
    }
}