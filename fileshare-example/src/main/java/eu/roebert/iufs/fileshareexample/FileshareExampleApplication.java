package eu.roebert.iufs.fileshareexample;

import static spark.Spark.port;
import static spark.Spark.staticFiles;
import static spark.Spark.threadPool;

import eu.roebert.iufs.core.IStartupComponents;
import eu.roebert.iufs.core.IUFSStartup;
import eu.roebert.iufs.fileshareexample.controller.DownloadFileControllerAgent;
import eu.roebert.iufs.fileshareexample.controller.UploadFileControllerAgent;
import eu.roebert.iufs.fileshareexample.tools.GlobalFilter;

public class FileshareExampleApplication {
    public final static int PORT = 80;
    private IUFSStartup iufs;
    private IStartupComponents starter;

    public static void main(String[] args) {
        new FileshareExampleApplication();
    }

    /**
     * After Bootup, open your browser to test: http://localhost:80
     */
    public FileshareExampleApplication() {
        iufs = new IUFSStartup();

        starter = iufs.getStarter();

        staticFiles.externalLocation("public/");
        staticFiles.expireTime(1);

        port(PORT);
        threadPool(500, 5, 30000);

        registerControllers();
    }

    /**
     * Register REST routes at the Spark framework.
     */
    private void registerControllers() {
        GlobalFilter.apply(); // Call this before mapping the routes

        starter.createComponent(UploadFileControllerAgent.class);
        starter.createComponent(DownloadFileControllerAgent.class);
    }
}
