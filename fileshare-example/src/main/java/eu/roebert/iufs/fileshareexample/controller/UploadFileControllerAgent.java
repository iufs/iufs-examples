package eu.roebert.iufs.fileshareexample.controller;

import static spark.Spark.post;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.core.IIUFSService;
import eu.roebert.iufs.model.FileContainer;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import spark.Request;
import spark.Response;

@Agent
@Service
@RequiredServices({
        @RequiredService(name = "iufsservice", type = IIUFSService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class UploadFileControllerAgent extends Controller {
    private static final Logger LOG = LoggerFactory.getLogger(UploadFileControllerAgent.class);
    public final static int MAX_FILE_SIZE = 2000 * 1024 * 1024; // 2000MB
    public final static int MAX_REQUEST_SIZE = 2500 * 1024 * 1024; // 2500MB
    public final static int BUFFER_SIZE = 4096;

    @AgentService(lazy = false)
    IIUFSService iufsservice;

    @AgentBody
    public void onAgentBody() {
        post("/upload", "application/json", this::getSparkLogic, json);

        LOG.info("{} Agent Started", this);
    }

    @Override
    public Object getSparkLogic(Request request, Response response) {
        request.attribute("org.eclipse.jetty.multipartConfig",
                new MultipartConfigElement("/temp", MAX_FILE_SIZE, MAX_REQUEST_SIZE, BUFFER_SIZE));

        paramsExist(request);
        Collection<Part> parts = null;
        ArrayList<String> ids = new ArrayList<String>();
        HashMap<String, ArrayList<String>> pair = new HashMap<>();

        try {
            parts = request.raw().getParts();

            for (Part part : parts) {
                File tempFile = File.createTempFile("tempFile_", part.getSubmittedFileName());
                FileUtils.copyInputStreamToFile(part.getInputStream(), tempFile);
                FileContainer file = new FileContainer(tempFile);

                ids.add(iufsservice.upload(file).getNextIntermediateResult().getKey().getId());
            }

            pair.put("ids", ids);

            model = pair;
        } catch (Exception e) {
            errors.add("An error occured while uploading your file.");
            e.printStackTrace();
        }

        return generateResponse(response);
    }
}