package eu.roebert.iufs.fileshareexample.tools;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 * Transform any class based on their getters in JSON. For this purpose the
 * class has to be JavaBean conform.
 */
public class JsonTransformer implements ResponseTransformer {

    private Gson gson = new Gson();

    @Override
    public String render(Object model) {
        return gson.toJson(model);
    }
}